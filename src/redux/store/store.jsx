import { configureStore } from '@reduxjs/toolkit';
import products from 'src/redux/reducers/productsReducer';
import modal from 'src/redux/reducers/modalReducer';
import formBuy from 'src/redux/reducers/formBuyReducer';

export const store = configureStore({
  reducer: {
    products,
    modal,
    formBuy,
  },
});
